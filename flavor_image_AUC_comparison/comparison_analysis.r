###############################################################
#Compare flavor and IMG results using Area Under the Curve
#J.R. Dalenberg, April 2016
#   v0.1 beta
#
#R version 3.1.3 (2015-03-09) -- "Smooth Sidewalk"
################################################################

### Initial package loading and variables
#Statistics
library(lme4)         #v1.1-7 
library(lmerTest)     #v2.0-25 
#Plotting
library(ggplot2)      #v1.0.1 
library(colorRamps)   #v2.3 
library(gplots)       #v2.17.0 
#(fmri) data manipulation
library(R.matlab)     #v3.2.0 
library(plyr)         #v1.8.2 
library(signal)       #v0.7-4 
#Multicore packages
library(foreach)      #v1.4.2 
library(doMC)         #v1.3.3 
#Permutation
library(permute)      #v0.8-4 

##Run script modules or not
createImageAUCdata = 0 #Creating datafile with AUC data cor the image paradigm?
createFlavorAUCdata = 0 #Creating datafile with AUC data cor the flavor paradigm?

#Initial params
numIC = 102 #Number of ICs from the group ICA toolbox (GIFT)
NumPreStimIMG = 2 #Number of pre stimulus onset time bins (1 per sec) for image paradigm
NumPostStimIMG = 20 #Number of post stimulus onset time bins (1 per sec) for image paradigm
NumPreStimFLA = 2 #Number of pre stimulus onset time bins (1 per sec) for flavor paradigm
NumPostStimFLA = 30 #Number of post stimulus onset time bins (1 per sec) for flavor paradigm

#Calculate peristimulus x axis values 
XaxisIMG = c((0-NumPreStimIMG):(0+NumPostStimIMG))
XaxisFLA = c((0-NumPreStimFLA):(0+NumPostStimFLA))

## Set directories and acquire data
setwd('/Users/jelledalenberg/Dropbox/TIFN_SL/VIP/fntsa/flavor_image_AUC_comparison/')
work_path = getwd() # Set analysis location

#Get IC info
ICnames = read.table('./IClabels_simple.txt', sep='\t', header = T)
names(ICnames) = c('IC', 'Label')

## Load data
load(file = '../image_analysis/TimeBinData.rdata')
cDatIMG = cDat
ICcolsIMG = c(14:115)

load(file = '../flavor_analysis/TimeBinData.rdata')
cDatFLA = cDat
ICcolsFLA = c(18:119)
remove(cDat)

########### Creating Datafile with AUC data
if(createImageAUCdata){
  Subs = as.character(unique(cDatIMG$Sub))
  cDatIMG_long = data.frame()
  for(curSub in Subs){
    #Get Runs
    allRuns  = unique(cDatIMG$Run[cDatIMG$Sub == curSub])
    #Get trials
    allTrials = unique(cDatIMG$Trial[cDatIMG$Sub == curSub])
    for(curTrial in allTrials){
      cat(paste('Calculating data of Subject:',curSub,"trial:",curTrial, '\n'))
      for(curIC in c(1:numIC)){
        
        tmpICdat = cDatIMG[cDatIMG$Sub == curSub &
                             cDatIMG$Trial == curTrial,ICcolsIMG[curIC]]
        
        curAUCval = MESS::auc(XaxisIMG, tmpICdat, from = 3, to = 9, type = 'spline')
        
        cDatIMG_long = rbind(cDatIMG_long, cbind(cDatIMG[cDatIMG$Sub == curSub &
                                                           cDatIMG$Trial == curTrial &
                                                           cDatIMG$Bin == 1, 1:6],
                                                 IC = curIC,
                                                 IC_AUCval = curAUCval))
      }
    }
  }
  
  save(cDatIMG_long, file = './ImageAUCdata.rdata')
} else {
  #Load timecourse data
  load(file = './ImageAUCdata.rdata')
}

if(createFlavorAUCdata){
  cDatFLA_long = data.frame()
  for(curSub in Subs){
    #Get Runs
    allRuns  = unique(cDatFLA$Run[cDatFLA$Sub == curSub])
    for(curRun in allRuns){
      #Get trials
      allTrials = unique(cDatFLA$Trial[cDatFLA$Sub == curSub])
      for(curTrial in allTrials){
        cat(paste('Calculating data of Subject:',curSub,'Run:', curRun,"trial:",curTrial, '\n'))
        for(curIC in c(1:numIC)){
          
          tmpICdat = cDatFLA[cDatFLA$Sub == curSub &
                               cDatFLA$Run == curRun &
                               cDatFLA$Trial == curTrial,ICcolsFLA[curIC]]
          
          curAUCval = MESS::auc(XaxisFLA, tmpICdat, from = 3, to = 9, type = 'spline')
          
          cDatFLA_long = rbind(cDatFLA_long, cbind(cDatFLA[cDatFLA$Sub == curSub &
                                                             cDatFLA$Run == curRun &
                                                             cDatFLA$Trial == curTrial &
                                                             cDatFLA$Bin == 1, 1:6],
                                                   IC = curIC,
                                                   IC_AUCval = curAUCval))
        }
      }
    }
  }
  save(cDatFLA_long, file = './FlavorAUCdata.rdata')
} else {
  #Load timecourse data
  load(file = './FlavorAUCdata.rdata')
}

# save(cDatIMG_long, file = "/Users/jelledalenberg/Dropbox/TIFN_SL/VIP/IMG/IMG_AUC_data_per_trial_per_IC.RDATA")
load(file = "/Users/jelledalenberg/Dropbox/TIFN_SL/VIP/IMG/IMG_AUC_data_per_trial_per_IC.RDATA")

#Scale ratings in IMG paradigm
subs = unique(cDatIMG_long$Sub)
cDatIMG_long$Liking_Scaled = as.numeric(NA)
cDatIMG_long$Intensity_Scaled = as.numeric(NA)
teller = 1
for (i in subs){ 
  #Store demeaned data
  cDatIMG_long$Liking_Scaled[cDatIMG_long$Sub == i] = scale(cDatIMG_long$Liking[cDatIMG_long$Sub == i])
  cDatIMG_long$Intensity_Scaled[cDatIMG_long$Sub == i] = scale(cDatIMG_long$Intensity[cDatIMG_long$Sub == i])
  #Go to next subject
  teller = teller + 1
}

#Scale ratings in flavor paradigm
subs = unique(cDatFLA_long$Sub)
cDatFLA_long$Liking_Scaled = as.numeric(NA)
cDatFLA_long$Intensity_Scaled = as.numeric(NA)
teller = 1
for (i in subs){ 
  #Store demeaned data
  cDatFLA_long$Liking_Scaled[cDatFLA_long$Sub == i] = scale(cDatFLA_long$Liking[cDatFLA_long$Sub == i])
  cDatFLA_long$Intensity_Scaled[cDatFLA_long$Sub == i] = scale(cDatFLA_long$Intensity[cDatFLA_long$Sub == i])
  #Go to next subject
  teller = teller + 1
}

#Check results to compare with previous analysis 
summary(lmer(IC_AUCval ~ Liking_Scaled + Intensity_Scaled + (1|Sub), dat = cDatFLA_long[cDatIMG_long$IC == 34,])) #Flavor PFC result
summary(lmer(IC_AUCval ~ Liking_Scaled + Intensity_Scaled + (1|Sub), dat = cDatIMG_long[cDatIMG_long$IC == 81,])) #Image PFC result
summary(lmer(IC_AUCval ~ Liking_Scaled + Intensity_Scaled + (1|Sub), dat = cDatIMG_long[cDatIMG_long$IC == 83,])) #Image PFC result
summary(lmer(IC_AUCval ~ Liking_Scaled + Intensity_Scaled + (1|Sub), dat = cDatIMG_long[cDatIMG_long$IC == 98,])) #Image PFC result

### Comparing FLAVOUR and IMG
#Combine datasets

cDatIMG_long$Condition = "IMG"
cDatFLA_long$Condition = "FLA"

#Fixing some stats
cDatIMG_long$Run = 5

cDat_long_all = rbind(cDatIMG_long, cDatFLA_long)
cDat_long_all$Condition = as.factor(cDat_long_all$Condition)

#FN34
Mod34a = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 34,])
Mod34b = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled*Condition + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 34,])
anova(Mod34a,Mod34b)
summary(Mod34b)
anova(Mod34b)

#FN81
Mod81a = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 81,])
Mod81b = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled*Condition + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 81,])
anova(Mod81a,Mod81b)
summary(Mod81b)
anova(Mod81b)

#FN83
Mod83a = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 83,])
Mod83b = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled*Condition + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 83,])
anova(Mod83a,Mod83b)
summary(Mod83b)
anova(Mod83b)

#FN98
Mod98a = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 98,])
Mod98b = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled*Condition + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 98,])
anova(Mod98a,Mod98b)
summary(Mod98b)
anova(Mod98b)

#Insula: FN84
Mod84a = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 84,])
Mod84b = lmer(IC_AUCval ~ Liking_Scaled*Condition + Intensity_Scaled*Condition + (1|Sub), dat = cDat_long_all[cDat_long_all$IC == 84,])
anova(Mod84a,Mod84b)
summary(Mod84b)
anova(Mod84b)
