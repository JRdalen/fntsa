###############################################################
#Stimulus locked functional network time series analysis
#J.R. Dalenberg, April 2016
#   v0.1 beta
#
#R version 3.1.3 (2015-03-09) -- "Smooth Sidewalk"
################################################################

### Initial package loading and variables
#Statistics
library(lme4)         #v1.1-7 
library(lmerTest)     #v2.0-25 
#Plotting
library(ggplot2)      #v1.0.1 
library(colorRamps)   #v2.3 
library(gplots)       #v2.17.0 
#(fmri) data manipulation
library(R.matlab)     #v3.2.0 
library(plyr)         #v1.8.2 
library(signal)       #v0.7-4 
#Multicore packages
library(foreach)      #v1.4.2 
library(doMC)         #v1.3.3 
#Permutation
library(permute)      #v0.8-4 

#Initial params
extractIcaTimeCourses = 0 #Have the time courses already been extracted from the GIFT ica output files?
CreateBoldDataBins = 0 #Have the BOLD time bins already been calculated per functional network?
plotAverageStimulusTrialperFN = 0 #Have average stimulus trials already been plotted?
runPermutations = 0 #Have Permuations already be run?
numPTests = 1000 #Number of permutations
alpha = 0.05 #Critical p-value level
TR = 2.45 #Set TR of the fMRI images.
HPF = 64 #Set high pass filter in seconds.
numIC = 102 #Number of ICs from the group ICA toolbox (GIFT)
NumPreStim = 2 #Number of pre stimulus onset time bins (1 per sec)
NumPostStim = 20 #Number of post stimulus onset time bins (1 per sec)
numCores = 7 #set number of cores/threads for statistical analysis

## Set directories and acquire data
setwd('/Users/jelledalenberg/Dropbox/TIFN_SL/VIP/fntsa/image_analysis/')
work_path = getwd() # Set analysis location
PfileParentDir = '/Volumes/LaCie/studies/VIP/data/' #Directory with original fMRI data

#Read IC names
ICnames = read.table('./IClabels_simple.txt', sep='\t', header = T)
names(ICnames) = c('IC', 'Label')

# Get ICA data location 
ica_path = '../../ICA_output_images/'

#Read stimulus data (onsets, ratings etc)
StimDat = read.table(file='./IMG_Bdata.txt',sep=',', header = T)
StimDat = arrange(StimDat, Sub, Run, Trial)

# Some initial color palettes for plotting
my_palette = blue2red(999)
my_palette2 = blue2green2red(999) 

#Computational efficient function to get t-values from mixed effect models.
t.stat = function(x) fixef(x)/sqrt(diag(vcov(x)))

#Determine the file seperator of the current platform.
filesep = .Platform$file.sep
#Create the Lowpass filter
bf = butter(4, (1/HPF)*TR, type="high")
#Save the default plot variables.
OldPar = par()
#Calculate peristimulus x axis values 
Xaxis = c((0-NumPreStim):(0+NumPostStim))

##  Extract data from matlab files ##
if(extractIcaTimeCourses){
  # Read data
  tc_files = dir(path = ica_path, pattern = 'ica_c.*-1.mat')
  
  #Order files correctly
  SubNums = mat.or.vec(length(tc_files),1)
  count = 1
  for(tc_file in tc_files){ SubNums[count] = as.numeric(unlist(strsplit(tc_files[count],split = '[c-]'))[3]); count = count + 1}
  sort_ind = sort.int(SubNums, index.return = T)$ix
  tc_files = tc_files[sort_ind]
  
  #Initialize ICA time course dataset
  dat = vector("list", length(tc_files))
  #Read ICA time course data
  count = 1
  for(tc_file in tc_files){
    cat(paste('Processing file:',tc_file,'\n'))
    dat[[count]] = as.data.frame(readMat(paste(ica_path, tc_file, sep=filesep))$tc)
    count = count + 1
  }
  save(dat, file = paste(work_path, 'Timecourses.rdata', sep=filesep))
} else {
  #Load timecourse data
  load(file = paste(work_path, 'Timecourses.rdata', sep=filesep))
}

######### Create 1s BOLD-data-bins per stimulus ########
if(CreateBoldDataBins){
  PfileParentDir = '/Volumes/Seagate_external/TIFN/VIP/data/'
  SubDirs = dir(path = PfileParentDir, pattern = '^s.*')
  SubBinDat = vector("list", length(SubDirs))
  #Select subject
  for(Sub in 1:length(SubDirs)){
    #Select data
    curStimDat = StimDat[StimDat$Sub == SubDirs[Sub],]
    
    #Feedback
    cat(paste('Calculating time bin data of Subject', SubDirs[Sub],'\n'))
    
    #Get dataframe
    curBOLDdat = as.matrix(dat[[Sub]])
    
    #Fitting splines
    curSplines = vector("list", length(numIC))
    for(curIC in 1:numIC){
      #Filtering
      CurBold_highpass = as.numeric(filter(bf, curBOLDdat[,curIC]))
      
      #Spline fitting
      curTimeVar = cumsum(rep(TR, length(curBOLDdat[,curIC])))-TR
      curSplines[[curIC]] = splinefun(curTimeVar,CurBold_highpass)
    }
    
    #Get datapoints to calculate bins per stimulus (per run per subject)
    #Select trial
    Trials = unique(curStimDat$Trial)
    SubBinDat[[Sub]] = vector("list", length(Trials))
    
    for(curTrial in Trials){
      #Get Trial Start Time
      curTrialStart = curStimDat$ImageStart[curStimDat$Trial == curTrial]
      
      if(length(curTrialStart) != 1 ){
        cat(paste('Trial Start Error here \n'))   
      }
      #Create time bins
      curBins = c((curTrialStart-NumPreStim):(curTrialStart+NumPostStim))
      
      #Create matrix that will contain the bin data inside the datastructure
      SubBinDat[[Sub]][[curTrial]] = mat.or.vec(numIC,NumPreStim+NumPostStim+1)
      #Calculate 1-second bins per IC around the stimulus
      for(curSpline in 1:length(curSplines)){
        SubBinDat[[Sub]][[curTrial]][curSpline,] = curSplines[[curSpline]](curBins)     
      }
    }
  }
  
  #Combine data
  cDat = data.frame()
  for(curBin in 1:length(Xaxis)){
    DF1 = mat.or.vec(dim(StimDat)[1],numIC)
    Ncount = 1
    for(s in 1:length(SubBinDat)){
      for(t in 1:length(SubBinDat[[s]])){
        DF1[Ncount,] = SubBinDat[[s]][[t]][,curBin]
        Ncount = Ncount + 1
      }   
    }
    cDat = rbind(cDat,cbind(StimDat, Bin = curBin, as.data.frame(DF1)))
  }
  cDat = arrange(cDat, Sub, Run, Trial, Bin)
  cDat$Bin = as.factor(cDat$Bin)
  
  save(cDat, file = paste(work_path, 'TimeBinData.rdata', sep=filesep))
} else {
  #Load timecourse data
  load(file = paste(work_path, 'TimeBinData.rdata', sep=filesep))
}

#Get bin total
binTot = NumPreStim+NumPostStim+1
#et voila! a data frame containing the time course for each component and each stimulus trial
cDat = arrange(cDat, Sub, Run, Trial, Bin)
cDat$Bin = as.factor(cDat$Bin) 

#Which are the columns of the ICs?
ICcols = c(14:115)

#Get bin total
binTot = NumPreStim+NumPostStim+1

######### Behavioral results ######
#Liking vs Intensity
boxplot(cDat$Intensity~cDat$Liking, ylim = c(0,10), xlim = c(0,10), main = "Liking vs Intensity")
plotDat = cDat
plotDat$Liking = as.factor(plotDat$Liking)
LikMod = lmer(Intensity ~ Liking + (1|Sub), dat = plotDat[plotDat$Bin == 1,])

LikModEst = fixef(LikMod)
LikModEst[2:9] = LikModEst[2:9]+LikModEst[1]
LikModCI = confint(LikMod, level = 0.95, method = 'profile')
LikModCI = LikModCI[3:11,1:2]
LikModCI[2:9,1:2] = LikModCI[2:9,1:2]+LikModEst[1]
LikModCI = as.data.frame(LikModCI)
names(LikModCI) = c("Low","High")
LikModCI$Intensity = LikModEst
LikModCI$Liking = 1:9

pdf(file = './plots/Intensity-Liking.pdf', width = 3, height = 3)
ggplot(LikModCI, aes(x=Liking, y=Intensity)) +
  geom_point(col = '#3446EA',size = 2) +
  geom_errorbar(aes(x=Liking, ymin=Low, ymax=High), width=0.5,col = '#3446EA', size = 1.1) +
  ggtitle("Image ratings") +
  scale_x_discrete(limits=c(1:9)) +
  scale_y_discrete(limits=c(1:9)) +
  theme_bw() 
dev.off()

freqs = as.data.frame((table(plotDat$Liking[plotDat$Bin == 1])))
names(freqs) = c("Liking","Frequency")
freqs

###### OVERALL BOLD Results per FN ####
### Valence Dummy Var ####
cDat$Valence = 0
cDat$Valence[cDat$Liking > 5] = 1
cDat$Valence[cDat$Liking < 5] = -1
cDat$Valence = as.factor(cDat$Valence)

####plotting average stimulus trial per component, split for positive and negative pleasantness.
if(plotAverageStimulusTrialperFN){
  plot_cDat = cDat[cDat$Valence != 0,]
  plot_cDat$Valence = as.factor(as.character(plot_cDat$Valence)) #Ignore neutral ratings (rating=4, on scale 1-9).
  
  for(curIC in 1:numIC){
    Y = mat.or.vec(binTot,length(unique(plot_cDat$Valence)))
    colnames(Y) = c("Neg","Pos")
    Yci = mat.or.vec(binTot,length(unique(plot_cDat$Valence))*2)
    colnames(Yci) = c("Neg_2.5","Neg_97.5","Pos_2.5","Pos_97.5")
    
    plot_cDat$curIC = plot_cDat[,ICcols[curIC]]
    
    for(curBin in 1:binTot){
      cat(paste('IC',curIC,'bin',curBin,'\n'))
      
      m01 = lmer(curIC ~ Valence + (1|Sub), dat = plot_cDat[plot_cDat$Bin == curBin,])
      
      Y[curBin,1] = fixef(m01)[1]
      Y[curBin,2] = fixef(m01)[2] + fixef(m01)[1]
      
      curConfint = confint(m01, level = 0.95, method = 'profile')[3:4,]
      Yci[curBin,1:2] = curConfint[1,1:2]
      Yci[curBin,3:4] = curConfint[2,1:2] + fixef(m01)[1]
    }
    
    pdf(file = paste('./IC_plots_IMG/IC',curIC,'_TC.pdf', sep = ""), width = 8, height = 5)
    plot(Xaxis, Y[,1], type = 'l', ylim = c(-1,1.5), ylab = paste('IC',curIC,' timecourse (a.u.)'),col="black",
         xlab = 'Time around stimulus (seconds)', main = paste('IC',curIC,'Time course around stimulus onset (VIP Study)'))
    polygon(c(rev(Xaxis),Xaxis),c(rev(Yci[,1]),Yci[,2]), col=rgb(1, 0, 0,0.3), border = NA)
    lines(Xaxis,Y[,2], col = "black")
    polygon(c(rev(Xaxis),Xaxis),c(rev(Yci[,3]),Yci[,4]), col=rgb(0,1,0,0.3), border = NA)
    dev.off()
  }
}

### Statistics on time courses ####
#Overall BOLD beta values across trials and participants (using mass linear mixed effect models)
registerDoMC(numCores) #set number of cores/threads
Result_mat = foreach(curIC = ICnames$IC, .combine=rbind) %dopar% {
  curICdat = mat.or.vec(1,binTot)
  for(curBin in 1:binTot){
    #cat(paste('IC',curIC,'bin',curBin,'\n'))
    #Set current IC
    cDat$curIC = cDat[,ICcols[curIC]] 
    #Run model for current IC and current Bin
    Val_m01 = lmer(curIC ~ 1 + (1|Sub), dat = cDat[cDat$Bin == curBin,])
    #Save intercept beta
    curICdat[curBin] = as.numeric(fixef(Val_m01))
  }
  curICdat
}
Result_mat = as.data.frame(Result_mat)
names(Result_mat) = Xaxis
Result_mat = as.matrix(Result_mat)
registerDoMC(1) # Set core usage back to 1

pdf(file = './IC_BOLD.pdf', width = 10, height = 10)
Bold_clust_order = heatmap(Result_mat, Colv = NA,col = my_palette,Rowv = NA,labRow = ICnames$Label,
                           scale = "none", main = "IC BOLD responses")
dev.off()

#Pleasantness (liking) models
#scale liking and intensity responses per subject
subs = unique(cDat$Sub)
cDat$Liking_Scaled = as.numeric(NA)
cDat$Intensity_Scaled = as.numeric(NA)
counter = 1
for (i in subs){ 
  #Store demeaned data
  cDat$Liking_Scaled[cDat$Sub == i] = scale(cDat$Liking[cDat$Sub == i])
  cDat$Intensity_Scaled[cDat$Sub == i] = scale(cDat$Intensity[cDat$Sub == i])
  #Go to next subject
  counter = counter + 1
}

#Run pleasantness models (adjusted for intensity)
registerDoMC(numCores)
Result_mat_L = foreach(curIC = ICnames$IC, .combine=rbind) %dopar% {
  curICdat = mat.or.vec(1,binTot)
  for(curBin in 1:binTot){ 
    cDat$curIC = cDat[,ICcols[curIC]]   
    Val_m01 = lmer(curIC ~ Liking_Scaled + Intensity_Scaled + (1|Sub), dat = cDat[cDat$Bin == curBin,])
    curICdat[curBin] = t.stat(Val_m01)[2]
  }
  curICdat
}
colnames(Result_mat_L) = Xaxis
registerDoMC(1)

### Permutation Thresholding.
# THIS MAY TAKE MORE THAN A WEEK (depending on # of cores and speed of system)!
if(runPermutations){
  #Permutations
  initTime = proc.time() #Get timestamp
  registerDoMC(numCores)
  maxTvalues = mat.or.vec(1,numPTests)
  pMats =  vector("list", numPTests)
  for(pTest in 1:numPTests){
    curTime = proc.time()
    cat(paste('Permutation:',pTest,'of',numPTests,'... '))
    #Relabeling data per subject
    cDat$pLiking = foreach(curSub = unique(cDat$Sub), .combine=c) %do% {
      #Create groups based on trial info
      Groups = cumsum(diff(c(-Inf,cDat$Trial[cDat$Sub == curSub]))!=0)
      #Use permutation package to permute the trials and randomize the liking ratings per subject.
      CTRL = how(within = Within(type = "free", mirror = TRUE),
                 plots = Plots(strata = Groups, type = "free"))
      cDat$Liking_Scaled[cDat$Sub == curSub][shuffle(length(Groups), control = CTRL)]
    }
    
    #Calculate the T values for all IC x Liking relation per timebin
    Result_mat_pL = foreach(curIC = c(1:numIC)[-BadICs], .combine=rbind) %dopar% {
      curICdat = mat.or.vec(1,binTot)
      for(curBin in 1:binTot){
        cDat$curIC = cDat[,ICcols[curIC]]
        Val_m01 = lmer(curIC ~ pLiking + Intensity_Scaled + (1|Sub), dat = cDat[cDat$Bin == curBin,])
        curICdat[curBin] = t.stat(Val_m01)[2]
      }
      curICdat
    }
    #Save results
    maxTvalues[pTest] = max(Result_mat_pL)
    pMats[[pTest]] = Result_mat_pL
    #Time
    cat(paste('Time:',round((proc.time() - curTime)[3],2),'sec \n'))
  }
  registerDoMC(1)
  save(pMats, file = "./FN_flavor_pleasantness_Tvalues_(adjusted_for_intensity)_pMats_1000.RDATA")
  
  #Time
  cat(paste('Total Time:',round((proc.time() - initTime)[3],2),'sec \n'))
} else {
  #load previously calculated permutations
  load("./FN_IMG_pleasantness_Tvalues_(adjusted_for_intensity)_pMats450.RDATA")
  Mat1 = pMats
  load("./FN_IMG_pleasantness_Tvalues_(adjusted_for_intensity)_pMats550.RDATA")
  Mat2 = pMats
}

#Set THR
critValMember = numPTests - (floor(alpha * numPTests))

#Global FWE THR
AllpMats =c( Mat1, Mat2)
AbsMaxT = mat.or.vec(1,numPTests)
for(i in 1:numPTests){
  AbsMaxT[i] = max(abs(AllpMats[[i]]))  
}
AbsMaxT_sorted = sort(AbsMaxT)
MaxTthr = AbsMaxT_sorted[critValMember+1]

#THR per component
AbsMaxT_perComp = mat.or.vec(dim(AllpMats[[1]])[1],numPTests)
for(i in 1:numPTests){
  AbsMaxT_perComp[,i] = apply((abs(AllpMats[[i]])), 1, function(x) max(x, na.rm = TRUE))
}
AbsMaxT_perComp_sorted = t(apply(t(AbsMaxT_perComp),2,sort))
MaxTthr_perComp = AbsMaxT_perComp_sorted[,critValMember+1]

#Start thresholding the Liking matrix based on THR per component
LikMat = Result_mat_L
for(i in 1:dim(LikMat)[1]){
  LikMat[i,abs(LikMat[i,]) < MaxTthr_perComp[i]] = 0
}

#Plot original pleasantness ratings
colors = c(seq(-5,5,length=1000))
pdf(file = './FN_IMG_pleasantness_Tvalues_(adjusted_for_intensity).pdf', width = 13.5, height = 10)
heatmap.2(Result_mat_L,dendrogram='none', Rowv=FALSE, Colv=FALSE,trace='none',
          density.info="none",col = my_palette2, scale = "none", labRow = ICnames$Label,
          main = paste("IMG PARADIGM
                       Relation IC ~ Pleasantness (corrected for Intensity)
                       Thresholded per component: P = 0.05 (",numPTests," permutations)",sep=''),
          lwid = c(1.5,5),
          lhei = c(.7,5),
          margins = c(4, 17),
          key.xlab="T value")
dev.off()

#Plot pleasantness with THR per FN
colors = c(seq(-5,5,length=1000))
pdf(file = './FN_IMG_pleasantness_Tvalues_(adjusted_for_intensity)_THRperFN.pdf', width = 13.5, height = 10)
heatmap.2(LikMat,dendrogram='none', Rowv=FALSE, Colv=FALSE,trace='none',
          density.info="none",col = my_palette2, scale = "none",
          main = paste("IMG PARADIGM
                       Relation IC ~ Pleasantness (corrected for Intensity)
                       Thresholded per component: P = 0.05 (",numPTests," permutations)",sep=''),
          lwid = c(1.5,5),
          lhei = c(.7,5),
          margins = c(4, 17),
          key.xlab="T value")
dev.off()
#Plot pleasantness with THR across all FNs (FWE)
LikMat2 = LikMat
LikMat2[abs(LikMat2) < MaxTthr] = 0
pdf(file = './FN_IMG_pleasantness_Tvalues_(adjusted_for_intensity)_FWE_THR.pdf', width = 13.5, height = 10)
heatmap.2(LikMat2,dendrogram='none', Rowv=FALSE, Colv=FALSE,trace='none',
          density.info="none",col = my_palette2, scale = "none",
          main = paste("IMG PARADIGM
                       Relation IC ~ Pleasantness (corrected for Intensity)
                       Thresholded per component: P = 0.05 (",numPTests," permutations)",sep=''),
          lwid = c(1.5,5),
          lhei = c(.7,5),
          margins = c(4, 17),
          key.xlab="T value")
dev.off()

#Get maximum T-values
LikmatTs = data.frame(ICnames, Tmax = apply(Result_mat_L[,6:12], 1, max), Tmin = apply(Result_mat_L[,6:12], 1, min))
absmax = function(x) { x[which.max( abs(x) )]}
LikmatTs$critval = apply(LikmatTs[,3:4], 1, absmax)
for (i in 1:length(LikmatTs$critval)){
  LikmatTs$pval[i] = length(which(AbsMaxT_sorted > abs(LikmatTs$critval[i])))/1000
}
LikmatTs[,5:6]

#### Plot the effects of pleasantness in prefrontally located ICs
#Plot T-values prefrontal.
pdf(file = './PFC_Tvalues.pdf', width = 7, height = 4)
ICs = c(34,81,83,98)
PlotDat = Result_mat_L[match(ICs, ICnames$IC),]
matplot(Xaxis[1:23],t(PlotDat)[1:23,], type = "l", col = c('green','violet','blue','red'),lty = 1, lwd = 2,
        ylim = c(-6,10),
        main = "IMAGES \n Liking effect (T values)", ylab = 'T value', xlab = 'Time (s)')
lines(Xaxis[1:23],rep(round(MaxTthr,2), 23), lty = 2, lwd = 2)
legend(18,9.8, c('IC34','IC81','IC83','IC98'), fill = c('green','violet','blue','red'), cex = .6)
text(0.5,4.7, label = paste("Tmax = ",round(MaxTthr,2),", P = 0.05 (corrected)",sep=''), cex = .5)
dev.off()