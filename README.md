### Stimulus locked functional network time series analysis ###
by J.R. Dalenberg, April 2016 - December 2017
v0.1 beta

Analysis requires R version version 3.1.3 (2015-03-09) -- "Smooth Sidewalk".

### The following libraries are used ###
### Statistics
lme4 v1.1-7 
lmerTest v2.0-25 
### Plotting
ggplot2 v1.0.1 
colorRamps v2.3 
gplots v2.17.0 
### (fmri)data manipulation
R.matlab v3.2.0 
plyr v1.8.2 
signal v0.7-4 
### Multicore packages
foreach v1.4.2 
doMC v1.3.3 
### Permutation
permute v0.8-4 
